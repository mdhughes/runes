#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright © 2019 by Mark Damon Hughes. All Rights Reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys

USAGE_TEXT = """Usage: runes.py [OPTION]
  OPTIONS:
    -l, -list		List all letter sets and symbols known in each

stdin-stdout filter. Replaces sequences of {LETTERSET:TEXT}, translating symbols in LETTERSET into Unicode.

Examples:
{runic:Motherfucking futhark, Viking times are here again!}
x{sub:y}
{arrow:^^vv<><>}

'reverse' set is special, reverses all known letter sets:
{reverse:↑↑↓↓←→←→}
"""

# letter set must be given in order, from digraphs down to single symbols.
LETTER_SETS = {
	"runic": [
		("AE", "ᛇ"),
		("EA", "ᛠ"),
		("NG", "ᛝ"),
		("ST", "ᛥ"),
		("TH", "ᚦ"),
		("A", "ᚨ"),
		("B", "ᛒ"),
		("C", "ᚳ"),
		("D", "ᛞ"),
		("E", "ᛖ"),
		("F", "ᚠ"),
		("G", "ᚷ"),
		("H", "ᚺ"),
		("I", "ᛁ"),
		("J", "ᛃ"),
		("K", "ᚲ"),
		("L", "ᛚ"),
		("M", "ᛗ"),
		("N", "ᛀ"),
		("O", "ᛟ"),
		("P", "ᛈ"),
		("Q", "ᛩ"),
		("R", "ᚱ"),
		("S", "ᛋ"),
		("T", "ᛏ"),
		("U", "ᚢ"),
		("V", "ᚡ"),
		("W", "ᚹ"),
		("X", "ᛪ"),
		("Y", "ᚣ"),
		("Z", "ᛉ"),
		(" ", "᛫"),
		(".", "᛬"),
		(",", "᛬"),
		(";", "᛬"),
		("!", "᛭"),
		("?", "᛭"),
	],
	"sub": [
		("RHO", "ᵨ"),
		("PHI", "ᵩ"),
		("CHI", "ᵪ"),
		("@", "ₔ"), # schwa
		("A", "ₐ"),
		("B", "ᵦ"), # beta
		("E", "ₑ"),
		("H", "ₕ"),
		("J", "ⱼ"),
		("K", "ₖ"),
		("L", "ₗ"),
		("M", "ₘ"),
		("N", "ₙ"),
		("N", "₏"), # alt
		("O", "ₒ"),
		("P", "ₚ"),
		("R", "ᵣ"),
		("S", "ₛ"),
		("T", "ₜ"),
		("U", "ᵤ"),
		("V", "ᵥ"),
		("X", "ₓ"),
		("Y", "ᵧ"), # gamma
		("0", "₀"),
		("1", "₁"),
		("2", "₂"),
		("3", "₃"),
		("4", "₄"),
		("5", "₅"),
		("6", "₆"),
		("7", "₇"),
		("8", "₈"),
		("9", "₉"),
		("+", "₊"),
		("-", "₋"),
		("=", "₌"),
		("(", "₍"),
		(")", "₎"),
	],
	"super": [
		("0", "⁰"),
		("1", "¹"),
		("2", "²"),
		("3", "³"),
		("4", "⁴"),
		("5", "⁵"),
		("6", "⁶"),
		("7", "⁷"),
		("8", "⁸"),
		("9", "⁹"),
		("+", "⁺"),
		("-", "⁻"),
		("=", "⁼"),
		("(", "⁽"),
		(")", "⁾"),
		("I", "ⁱ"),
		("N", "ⁿ"), # alt
	],
	"frac": [
		("1/10", "⅒"),
		("1/2", "½"),
		("0/3", "↉"),
		("1/3", "⅓"),
		("2/3", "⅔"),
		("1/4", "¼"),
		("3/4", "¾"),
		("1/5", "⅕"),
		("2/5", "⅖"),
		("3/5", "⅗"),
		("4/5", "⅘"),
		("1/6", "⅙"),
		("5/6", "⅚"),
		("1/7", "⅐"),
		("1/8", "⅛"),
		("3/8", "⅜"),
		("5/8", "⅝"),
		("7/8", "⅞"),
		("1/9", "⅑"),
		("1/", "⅟"),
	],
	"arrow": [
		("^", "↑"),
		("v", "↓"),
		("<", "←"),
		(">", "→"),
	],
	"mac": [
		("CMD-", "⌘"),
		("CTRL-", "⌃"),
		("OPT-", "⌥"),
		("SHIFT-", "⇧"),
		("BACKSPACE", "⌫"),
		("DELETE", "⌦"),
		("EJECT", "⏏"),
		("KEYBOARD", "⌨"),
		("POWER", "⎋"),
		("RETURN", "⏎"),
	],
	"circle": [
		("10", "⑩"),
		("11", "⑪"),
		("12", "⑫"),
		("13", "⑬"),
		("14", "⑭"),
		("15", "⑮"),
		("16", "⑯"),
		("17", "⑰"),
		("18", "⑱"),
		("19", "⑲"),
		("20", "⑳"),
		("21", "㉑"),
		("22", "㉒"),
		("23", "㉓"),
		("24", "㉔"),
		("25", "㉕"),
		("26", "㉖"),
		("27", "㉗"),
		("28", "㉘"),
		("29", "㉙"),
		("30", "㉚"),
		("31", "㉛"),
		("32", "㉜"),
		("33", "㉝"),
		("34", "㉞"),
		("35", "㉟"),
		("36", "㊱"),
		("37", "㊲"),
		("38", "㊳"),
		("39", "㊴"),
		("40", "㊵"),
		("41", "㊶"),
		("42", "㊷"),
		("43", "㊸"),
		("44", "㊹"),
		("45", "㊺"),
		("46", "㊻"),
		("47", "㊼"),
		("48", "㊽"),
		("49", "㊾"),
		("50", "㊿"),
		("0", "⓪"),
		("1", "①"),
		("2", "②"),
		("3", "③"),
		("4", "④"),
		("5", "⑤"),
		("6", "⑥"),
		("7", "⑦"),
		("8", "⑧"),
		("9", "⑨"),
		("A", "Ⓐ"),
		("B", "Ⓑ"),
		("C", "Ⓒ"),
		("D", "Ⓓ"),
		("E", "Ⓔ"),
		("F", "Ⓕ"),
		("G", "Ⓖ"),
		("H", "Ⓗ"),
		("I", "Ⓘ"),
		("J", "Ⓙ"),
		("K", "Ⓚ"),
		("L", "Ⓛ"),
		("M", "Ⓜ"),
		("N", "Ⓝ"),
		("O", "Ⓞ"),
		("P", "Ⓟ"),
		("Q", "Ⓠ"),
		("R", "Ⓡ"),
		("S", "Ⓢ"),
		("T", "Ⓣ"),
		("U", "Ⓤ"),
		("V", "Ⓥ"),
		("W", "Ⓦ"),
		("X", "Ⓧ"),
		("Y", "Ⓨ"),
		("Z", "Ⓩ"),
		("+", "⊕"),
		("-", "⊖"),
		("*", "⊗"),
		("/", "⊘"),
		(".", "⊙"),
		("=", "⊜"),
		("|", "⦶"),
	],
	"negative": [
		("10", "❿"),
		("11", "⓫"),
		("12", "⓬"),
		("13", "⓭"),
		("14", "⓮"),
		("15", "⓯"),
		("16", "⓰"),
		("17", "⓱"),
		("18", "⓲"),
		("19", "⓳"),
		("20", "⓴"),
		("0", "⓿"),
		("1", "❶"),
		("2", "❷"),
		("3", "❸"),
		("4", "❹"),
		("5", "❺"),
		("6", "❻"),
		("7", "❼"),
		("8", "❽"),
		("9", "❾"),
		("A", "🅰"),
		("B", "🅱"),
		("C", "🅲"),
		("D", "🅳"),
		("E", "🅴"),
		("F", "🅵"),
		("G", "🅶"),
		("H", "🅷"),
		("I", "🅸"),
		("J", "🅹"),
		("K", "🅺"),
		("L", "🅻"),
		("M", "🅼"),
		("N", "🅽"),
		("O", "🅾"),
		("P", "🅿"),
		("Q", "🆀"),
		("R", "🆁"),
		("S", "🆂"),
		("T", "🆃"),
		("U", "🆄"),
		("V", "🆅"),
		("W", "🆆"),
		("X", "🆇"),
		("Y", "🆈"),
		("Z", "🆉"),
	],
}

def runeList():
	for lset in sorted(LETTER_SETS.keys()):
		text = lset+":"
		for p in LETTER_SETS[lset]:
			text += " "+p[0]
		print(text)
	return 0

def runeTranslateSegment(line, reverse=False):
	colon = line.find(":")
	if colon <= 0:
		return "{"+line+"}"
	lset = line[0:colon]
	line = line[colon+1:]
	if lset == "reverse":
		for lset in LETTER_SETS:
			for p in LETTER_SETS[lset]:
				line = line.replace(p[1], p[0])
	else:
		if lset not in LETTER_SETS:
			return "{"+line+"}"
		for p in LETTER_SETS[lset]:
			line = line.replace(p[0], p[1])
			line = line.replace(p[0].lower(), p[1])
	return line

def runeTranslate(intext):
	i = 0
	text = ""
	while i < len(intext):
		c = intext[i]
		if c != "{":
			text += c
		else:
			endbrace = intext.find("}", i)
			if endbrace < 0:
				text += "{"
			else:
				text += runeTranslateSegment(intext[i+1:endbrace])
				i = endbrace
		i += 1
	return text

def usage():
	print(USAGE_TEXT, file=sys.stderr)
	sys.exit(1)

def main(argv):
	i = 0
	while i < len(argv):
		a = argv[i]
		i += 1
		if a.startswith("-"):
			if a == "-l" or a == "-list":
				return runeList()
			else:
				usage()
		else:
			usage()
	sys.stdout.write(runeTranslate(sys.stdin.read()) )
	sys.stdout.flush()
	return 0

if __name__ == "__main__":
	rc = main(sys.argv[1:])
	exit(rc)
