# Runes
###### Copyright © 2019 by Mark Damon Hughes. All Rights Reserved.
###### BSD 3-Clause License, see LICENSE

A text filter to convert ASCII sequences into nice Unicode or emoji. Call it from your favorite editor, or on the command line:

```
% echo "BEFORE {circle:this is some hollow text.} AFTER" |runes.py
BEFORE ⓉⒽⒾⓈ ⒾⓈ ⓈⓄⓂⒺ ⒽⓄⓁⓁⓄⓌ ⓉⒺⓍⓉ⊙ AFTER
```

### Usage

```
Usage: runes.py [OPTION]
  OPTIONS:
    -l, -list		List all letter sets and symbols known in each
```

stdin-stdout filter. Replaces sequences of {LETTERSET:TEXT}, translating symbols in LETTERSET into Unicode. Symbols may be written uppercase or lowercase, but multi-letter symbols may not be mixed-case.

### Examples

```
{runic:Motherfucking futhark, Viking times are here again!}
x{sub:y}
{arrow:^^vv<><>}

becomes:

ᛗᛟᚦᛖᚱᚠᚢᚳᚲᛁᛝ᛫ᚠᚢᚦᚨᚱᚲ᛬᛫ᚡᛁᚲᛁᛝ᛫ᛏᛁᛗᛖᛋ᛫ᚨᚱᛖ᛫ᚺᛖᚱᛖ᛫ᚨᚷᚨᛁᛀ᛭
xᵧ
↑↑↓↓←→←→

'reverse' set is special, reverses all known letter sets:
{reverse:↑↑↓↓←→←→}

becomes:

^^vv<><>
```

### Current Lettersets

(from `runes.py -l`)

```
arrow: ^ v < >
circle: 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z + - * / . = |
frac: 1/10 1/2 0/3 1/3 2/3 1/4 3/4 1/5 2/5 3/5 4/5 1/6 5/6 1/7 1/8 3/8 5/8 7/8 1/9 1/
mac: CMD- CTRL- OPT- SHIFT- BACKSPACE DELETE EJECT KEYBOARD POWER RETURN
negative: 10 11 12 13 14 15 16 17 18 19 20 0 1 2 3 4 5 6 7 8 9 A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
runic: AE EA NG ST TH A B C D E F G H I J K L M N O P Q R S T U V W X Y Z   . , ; ! ?
sub: RHO PHI CHI @ A B E H J K L M N N O P R S T U V X Y 0 1 2 3 4 5 6 7 8 9 + - = ( )
super: 0 1 2 3 4 5 6 7 8 9 + - = ( ) I N
```

### Using With BBEdit

For BBEdit, if you've saved runes.py in SOMEWHERE:

```
% mkdir -p "$HOME/Library/Application Support/BBEdit/Text Filters"
% cd "$HOME/Library/Application Support/BBEdit/Text Filters"
% ln -s SOMEWHERE/runes.py
```

And now it's in the Text, Apply Text Filters menu.

### Future

I'm not interested in covering all characters, just the ones I want to use regularly and can't easily find or type. Emoji in particular have a usable enough palette that I won't bother for most.

- latin accents
- greek
- mathematical symbols
- box drawing, etc.

###### EOF
